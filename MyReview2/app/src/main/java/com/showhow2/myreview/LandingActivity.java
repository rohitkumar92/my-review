package com.showhow2.myreview;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class LandingActivity extends ActionBarActivity implements AdapterView.OnItemClickListener{

    ListView l;
    String[] items={"Hello","My Reviews","Welcome"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

       l= (ListView) findViewById(R.id.reviews);

        ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,items);

        l.setAdapter(adapter);

       l.setOnItemClickListener(this);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){

            case R.id.action_add:

                Intent intent=new Intent(LandingActivity.this,AddVideoActivty.class);
                startActivity(intent);
                break;
            case R.id.action_logout:
                Intent intent1=new Intent(LandingActivity.this,LoginActivity.class);
                startActivity(intent1);



        }


        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long l) {

        TextView temp=(TextView) view;

        Toast.makeText(this,temp.getText()+""+i,Toast.LENGTH_SHORT).show();

    }



}
